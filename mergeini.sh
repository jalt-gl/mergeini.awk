#!/bin/bash

# ddraw.ini
#base="./example_inis/ddraw.modderspack.ini"
#override="./example_inis/ddraw.fo1in2.ini"
#merged="./out/ddraw.merged.ini"
#goal="./example_inis/ddraw.ini"

# f2_res.ini
#base="./example_inis/f2_res.HRP.ini"
#override="./example_inis/f2_res.fo1in2.ini"
#merged="./out/f2_res.merged.ini"
#goal="./example_inis/f2_res.ini"

# Fallout2.cfg
#base="./example_inis/fallout2.Fo2.cfg"
#override="./example_inis/Fallout2.fo1in2.cfg"
#merged="./out/Fallout2.merged.cfg"
#goal="./example_inis/Fallout2.cfg"

# sfall-mods.ini
base="./example_inis/sfall-mods.modderspack.ini"
override="./example_inis/sfall-mods.fo1in2.ini"
merged="./out/sfall-mods.merged.ini"
goal="./example_inis/sfall-mods.ini"


#awk -v override_file="${override}" -v adornments=inline -f mergeini.awk "${base}" > ${merged}
#meld "${merged}" "${goal}"

awk -v override_file="${override}" -v adornments=inline -f mergeini.awk "${base}"
